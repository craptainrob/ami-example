import React, { Component } from "react";
import "./App.css";
import vid from "./video.mp4";
import TextScroller from "./TextScroller.js";

/**
 * A button that shows itself when nothing is playing
 */
function PlayButton(props) {
  return (
    !props.isPlaying && (
      <div className="Center">
        <button className="PlayButton" onClick={props.onPlay}>
          Play
        </button>
      </div>
    )
  );
}

/**
 * Plays a video and then goes away
 */
function VideoPlayer(props) {
  return (
    props.isPlaying && (
      <div>
        <video
          className="VideoPlayer"
          src={props.src}
          autoPlay={true}
          onEnded={props.onComplete}
          width={1280}
          heightg={720}
        />
        <TextScroller
          messages={[
            { text: "Playing Today's Hits!", delay: 0 },
            { text: "Download the App", delay: 10 },
            { text: "Available on iOS and Android", delay: 20 }
          ]}
          speed={72}
        />
      </div>
    )
  );
}

/**
 * The top level container for this webapp
 */
export default class App extends Component {
  state = { isPlaying: false };

  /**
   * Play button clicked event handler
   */
  handlePlay = () => {
    this.setState({ isPlaying: true });
  };

  /**
   * Video ended event handler
   */
  handleVideoComplete = () => {
    this.setState({ isPlaying: false });
  };

  /**
   * React render function
   */
  render() {
    return (
      <div className="App">
        <PlayButton isPlaying={this.state.isPlaying} onPlay={this.handlePlay} />
        <VideoPlayer
          src={vid}
          isPlaying={this.state.isPlaying}
          onComplete={this.handleVideoComplete}
        />
      </div>
    );
  }
}
