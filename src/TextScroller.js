import React, { Component } from "react";
import { TweenLite, Power0 } from "gsap";

/**
 * Scrolls messages on a canvas
 */
export default class TextScroller extends Component {
  font = "50px sans-serif";

  componentDidMount() {
    TweenLite.ticker.addEventListener("tick", this.handleTick);
    this.ctx = this.target.getContext("2d");
    this.messages = this.props.messages;
    this.messages.map(this.initMessage);
  }

  componentWillUnmount() {
    TweenLite.ticker.removeEventListener("tick", this.handleTick);
  }

  /**
   * Begin scrolling a message
   */
  initMessage = msg => {
    this.ctx.font = this.font;
    let width = this.ctx.measureText(msg.text).width;

    TweenLite.fromTo(
      msg,
      (1280 + width) / this.props.speed,
      { xpos: 1280 },
      { xpos: -width, delay: msg.delay, ease: Power0.easeNone }
    );
  };

  /**
   * Draw a message to the canvas
   */
  drawMessage = msg => {
    this.ctx.fillStyle = "white";
    this.ctx.font = this.font;
    this.ctx.fillText(msg.text, msg.xpos, 720 * 0.66);
  };

  /**
   * GSAP update tick
   */
  handleTick = () => {
    this.ctx.clearRect(0, 0, 1280, 720);
    this.messages.map(this.drawMessage);
  };

  /**
   * Saves the canvas element as a ref for later use
   */
  handleRef = target => {
    this.target = target;
  };

  render() {
    return <canvas className="VideoOverlay" width={1280} height={720} ref={this.handleRef} />;
  }
}
